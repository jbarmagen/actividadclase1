import java.sql.*


class DataBaseAdmin2 {

    var sBaseDeDatos:String=""
    var sUser:String=""
    var sPassword:String=""
    var conn:Connection?=null

    constructor(baseDeDatos:String,user:String,password:String){
        this.sBaseDeDatos=baseDeDatos
        this.sUser=user
        this.sPassword=password

    }

    fun conectar(){
        val sUrl:String="jdbc:postgresql://rogue.db.elephantsql.com:5432/"+sBaseDeDatos
        try{
            conn=DriverManager.getConnection(sUrl,sUser,sPassword)
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    fun leerProductos():ArrayList<Producto>{
        var arProducto= arrayListOf<Producto>()
        try{
            var stmt:Statement=conn!!.createStatement()
            var result:ResultSet=stmt.executeQuery("SELECT * FROM \"public\".\"Productos\" LIMIT 100")
            while (result.next()==true){
                var productoTemp:Producto=Producto(
                        result.getInt("IDProducto"),
                        result.getString("Nombre"),
                        result.getInt("IDProveedor"),
                        result.getString("Ingredientes")
                )
                arProducto.add(productoTemp)
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
        return arProducto
    }


}